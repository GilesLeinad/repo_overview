use clap::Parser;
use std::path::Path;
use std::{env, fs, io};
use std::process::Command;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Root directory of the repositories
    #[arg(short, long, default_value_t = String::from(""))]
    directory: String,

    /// Show all local branches
    #[arg(short, long, default_value_t = false)]
    all_local_branches: bool,
}

fn main() -> io::Result<()> {
    let args = Args::parse();

    dbg!(&args);

    let _path = Path::new(&args.directory);

    let working_dir = env::current_dir().unwrap();

    dbg!(&working_dir);

    let entries = fs::read_dir(&working_dir)?;

    for entry in entries {
        if entry.as_ref().unwrap().path().is_dir() {
            let res = env::set_current_dir(entry.as_ref().unwrap().path());
            match res {
                Ok(_) => {
                    dbg!(env::current_dir().unwrap());

                    let output = String::from_utf8(Command::new("git").arg("status").output()?.stdout).unwrap();
                    dbg!(output);
                }
                Err(e) => {
                    print!(
                        "Can't change directory to: {}",
                        entry.as_ref().unwrap().path().display()
                    );
                    print!("Error: {}", e)
                }
            }
        }
    }
    let _res = env::set_current_dir(&working_dir);
    dbg!(env::current_dir().unwrap());

    Ok(())
}
